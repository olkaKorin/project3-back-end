const mongoose = require("mongoose");

const FighterSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  source: {
    type: String,
    required: true
  },
  attack: {
    type: Number,
    required: true
  },
  defense: {
    type: Number,
    required: true
  },
  health: {
    type: Number,
    required: true
  }
});

module.exports = mongoose.model("Fighter", FighterSchema);
