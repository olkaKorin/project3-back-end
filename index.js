const mongoose = require("mongoose");
const cors = require("cors");
const express = require("express");
const bodyParser = require("body-parser");
const {
  DB_USERNAME,
  DB_PASS,
  DB_HOST,
  DB_PORT,
  DB_NAME
} = require("./config.js");
const router = require("./router");
const { PORT = 5000 } = process.env;
//Hello
const startServer = async () => {
  try {
    mongoose.connect(
      `mongodb://${DB_USERNAME}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}`,
      { useNewUrlParser: true }
    );

    const app = express();
    app.use(cors());
    app.use(bodyParser());
    app.use("/", router);
    app.listen({ port: PORT }, () => {
      console.log(`Server ready at http://localhost:${PORT}`);
    });
  } catch (err) {
    console.error(err);
  }
};

startServer();
