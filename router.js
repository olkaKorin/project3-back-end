const express = require("express");
const router = express.Router();
const Fighter = require("./fighter.model");

router.get("/fighters", async (req, res) => {
  try {
    const fighters = await Fighter.find({}, "_id source name");
    res.status(200).json(fighters);
  } catch (error) {
    throw error;
  }
});

router.post("/fighter", async (req, res) => {
  try {
    const { source, _id, name } = await new Fighter(req.body).save();
    res.status(200).json({
      source,
      _id,
      name
    });
  } catch (error) {
    throw error;
  }
});

router.get("/fighter/:id", async (req, res) => {
  try {
    const fighter = await Fighter.findById(req.params.id);
    res.status(200).json(fighter);
  } catch (error) {
    throw error;
  }
});

router.delete("/fighter/:id", async (req, res) => {
  try {
    await Fighter.findOneAndDelete({ _id: req.params.id });
    const response = {
      message: "User has successfully been deleted",
      _id: req.params.id
    };
    return res.status(200).json(response);
  } catch (error) {
    throw error;
  }
});

router.put("/fighter", async (req, res) => {
  try {
    const updatedFighter = await Fighter.findByIdAndUpdate(
      { _id: req.body._id },
      req.body,
      { new: true }
    );
    res.status(200).json(updatedFighter);
  } catch (error) {
    throw error;
  }
});

module.exports = router;
